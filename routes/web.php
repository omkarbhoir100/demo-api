<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/config', function () use ($router) {
    // return $router->app->version();
    $response = [];

    $response = [
        'status'    => 200,
        'message'   => 'success',
        'data'      => [
            'version' => $router->app->version(),
            'project' => 'lumen',
            'token'   => 546546416515665691,
        ]
    ];

    return $response;
});
